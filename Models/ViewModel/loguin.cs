﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Personas.Models.ViewModel
{
    public class loguin
    {
        [Required(ErrorMessage = "Se requiere usuario.")]
        public string user { get; set; }
        [Required(ErrorMessage = "Se requiere password.")]
        public string pass { get; set; }
    }
}