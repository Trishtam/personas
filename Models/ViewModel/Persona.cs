﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Personas.Models.ViewModel
{
    public class Persona
    {
        public int id_Persona { get; set; }
        [Required(ErrorMessage = "Se requiere Nombre.")]
        public string Prim_Nombre { get; set; }
        public string Sec_Nombre { get; set; }
        
        [Required(ErrorMessage = "Se requiere Apellido.")]
        public string Prim_Apellido { get; set; }
        public string Sec_Apellido { get; set; }
        
        [Required(ErrorMessage = "Se requiere numero de identificacion.")]
        public string NumeroIdentificacion { get; set; }
        
        [Required(ErrorMessage = "El correo es necesario.")]
        public string Email { get; set; }
        public string TipoIdentificacion { get; set; }
        public System.DateTime FechaCreacion { get; set; }
        public string Concat_Identificacion { get; set; }
        public string Concat_Nombres { get; set; }

    
    }

    public class PersonaResponse
    {
        
        public string Concat_Identificacion { get; set; }
        public string Concat_Nombres { get; set; }
        
        public string FechaCreacion { get; set; }
        public string Respuesta { get; set; }

    }


    public class response
    {
        public string Respuesta { get; set; }
        public int codError { get; set; }

    }
}