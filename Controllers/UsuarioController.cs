﻿using Personas.Models;
using Personas.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Personas.Controllers
{
    [RoutePrefix("api/usuario")]
    public class UsuarioController : ApiController
    {
        /// <summary>
        /// Metodo para insertar nuevos usuarios
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("nuevo")]
        public response InsertUser(Usuario usuario)
        {

            response response = new response();
            try
            {
                using (MichaelPageTestEntities db = new MichaelPageTestEntities())
                {
                    var validate = db.tbl_Usuario.Where(l => l.usu_Usuario == usuario.user).FirstOrDefault();

                    if (validate != null)
                    {
                        response.Respuesta = "El usuario ya existe.";
                        
                        return response;
                    }


                    var objUser = new tbl_Usuario();
                    objUser.usu_Usuario = usuario.user;
                    objUser.usu_Pass = usuario.pass;

                    objUser.usu_FechaCreacion= DateTime.Now;

                    db.tbl_Usuario.Add(objUser);
                    db.SaveChanges();
                    response.Respuesta = "Usuario Ingresado.";
                }
            }
            catch (Exception)
            {

                throw new Exception(response.Respuesta.ToString());
            }

            return response;
        }


        /// <summary>
        /// Metodo para listar todos los usuarios
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAll")]
        public List<Usuario> getAll()
        {
            PersonaResponse resp = new PersonaResponse();
            try
            {
                using (MichaelPageTestEntities db = new MichaelPageTestEntities())
                {
                    List<Usuario> lstUser = new List<Usuario>();
                    var x = db.sp_getAllUsers();
                    if (x != null)
                    {
                        foreach (var us in x)
                        {
                            Usuario objUsr = new Usuario();
                            objUsr.user = us.usu_Usuario;
                            objUsr.pass = us.usu_Pass;

                            lstUser.Add(objUsr);
                        }
                    }
                    else
                    {
                        resp.Respuesta = "Sin Datos.";
                        throw new Exception(resp.Respuesta);
                    }


                    return lstUser;

                }
            }
            catch (Exception)
            {
                throw new Exception(resp.Respuesta);
            }
        
        }

    }
}
