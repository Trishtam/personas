﻿using System.Linq;
using System.Web.Http;
using Personas.Models;
using Personas.Models.ViewModel;

namespace Personas.Controllers
{
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpPost]
        [Route("ingresar")]
        public IHttpActionResult Authenticate(loguin login)
        {

            using (MichaelPageTestEntities db= new MichaelPageTestEntities())
            {   
                var validate = db.tbl_Usuario.Where(l => l.usu_Usuario == login.user && l.usu_Pass == login.pass).FirstOrDefault();
                
                if (validate != null)
                {
                    return Ok("Concedido");
                }
                else
                {                    
                    return Unauthorized();
                }
            }
            
        }

    }
}
