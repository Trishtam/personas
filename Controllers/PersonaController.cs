﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Personas.Models;
using Personas.Models.ViewModel;

namespace Personas.Controllers
{
    [RoutePrefix("api/personas")]
    public class PersonaController : ApiController
    {
        /// <summary>
        /// Metodo para insertar nuevos usuarios
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("nuevo")]
        public response InsertPerson(Persona people) {
            
            response response = new response();
            try
            {
                using (MichaelPageTestEntities db = new MichaelPageTestEntities())
                {
                    var validate = db.tbl_Persona.Where(l => l.per_NumeroIdentificacion == people.NumeroIdentificacion && l.per_TipoIdentificacion == people.TipoIdentificacion).FirstOrDefault();

                    if (validate != null)
                    {
                        response.Respuesta = "La persona ya existe.";
                        return response;
                    }

                    var oPer = new tbl_Persona();
                    oPer.per_Prim_Nombre = people.Prim_Nombre;
                    oPer.per_Sec_Nombre = people.Sec_Nombre;
                    oPer.per_Prim_Apellido = people.Prim_Apellido;
                    oPer.per_Sec_Apellido = people.Sec_Apellido;
                    oPer.per_NumeroIdentificacion = people.NumeroIdentificacion;
                    oPer.per_Email = people.Email;
                    oPer.per_TipoIdentificacion = people.TipoIdentificacion;
                    oPer.per_FechaCreacion = DateTime.Now;

                    db.tbl_Persona.Add(oPer);
                    db.SaveChanges();
                    response.Respuesta = "Ingresado.";
            }
            }
            catch (Exception )
            {
                               
                throw new Exception(response.Respuesta.ToString());
            }
            
            return response;
        }

        /// <summary>
        /// Metodo para listar todos las Personas
        /// </summary>
        /// <param name="people"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("getAll")]
        public List<PersonaResponse> getAll()
        {
            PersonaResponse resp = new PersonaResponse();
            try
            {
                using (MichaelPageTestEntities db = new MichaelPageTestEntities())
                {
                    List<PersonaResponse> lstPersona = new List<PersonaResponse>();


                    var x = db.sp_GetAllPersons();
                    if (x != null)
                    {
                        foreach (var pe in x)
                        {
                            PersonaResponse objPer = new PersonaResponse();
                            objPer.Concat_Identificacion = pe.per_Concat_Identificacion;
                            objPer.Concat_Nombres = pe.per_Concat_Nombres;
                            objPer.FechaCreacion = pe.per_FechaCreacion.ToString("d");
                            objPer.Respuesta = "Ok";
                            lstPersona.Add(objPer);
                        }
                    }
                    else
                    {   
                        resp.Respuesta = "Sin Datos.";
                        throw new Exception(resp.Respuesta);
                    }


                    return lstPersona;

                }
            }
            catch (Exception)
            {

                
                throw new Exception(resp.Respuesta);
            }

        }

    }
}
